<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_esc_css( $class ) {

	if ( ! $class )
		return false;

	return wp_kses ( str_replace( '_', '-', $class ), 'string' );

}

function tif_sanitize_key( $key ) {

	$key = remove_accents( $key );
	$key = sanitize_key( $key );

	return trim( $key, '-' );

}

function tif_sanitize_slug( $slug ) {

	$slug = strtolower( $slug );
	$slug = remove_accents( $slug );
	$slug = str_replace( array( ' ', '_' ), '-', $slug );
	$slug = preg_replace( '/[^a-z0-9 -]+/', '', $slug );

	return trim( $slug, '-' );

}

function tif_sanitize_filename( $filename ) {

	// Prevent empty filename
	if ( empty( $filename ) )
		return $filename;

	// get extension
	preg_match( '/\.[^\.]+$/i', $filename, $ext );

	// Exit if no extension
	if ( ! isset( $ext[0] ) )
		return $filename;

	// Get only first part
	$ext = $ext[0];

	// Get file name without extension
	$filename = str_replace( $ext, '', $filename );

	// only lowercase
	// remove accents
	$filename = sanitize_file_name( $filename );
	$filename = sanitize_title( $filename );

	// replace _ by -
	$filename = str_replace( '_', '-', $filename );

	// replace -- by -
	$filename = str_replace( '--', '-', $filename);

	return trim( $filename . $ext, '-' );

}

function tif_sanitize_date( $date, $format = 'Y-m-d' ) {

	$d = DateTime::createFromFormat($format, $date);

	return $d && $d->format($format) === $date ? $date : null ;

}

function tif_sanitize_time( $time ) {

	return (bool)strtotime( $time ) ? $time : null ;

}

function tif_sanitize_checkbox( $checked ) {
	// Boolean check.
	return ( ( isset( $checked ) && true == $checked ) ? true : false );
}

function tif_sanitize_multicheck( $values ) {

	$values = ( null == $values ) ? array() : $values;
	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	return ! empty( $values ) ? array_map( 'sanitize_text_field', $values ) : array();

}

function tif_sanitize_cover_fit( $values ) {

	// $values = ( null == $values ) ? array() : $values;
	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	$layout_array  = array(
		'default',
		'contain',
		'cover',
	);

	$position_array  = array(
		'left top',
		'center top',
		'right top',
		'left center',
		'center center',
		'center',
		'right center',
		'left bottom',
		'center bottom',
		'right bottom',
	);

	$unit_array  = array(
		'px',
		// 'rem',
		'vh',
		'%',
	);

	$layout          = isset( $values[0] ) && in_array( $values[0], $layout_array ) ? $values[0] : 'default';
	$position        = isset( $values[1] ) && in_array( $values[1], $position_array ) ? $values[1] : 'center center';
	$height          = isset( $values[2] ) ? max ( 0, $values[2] ) : '100';
	$unit            = isset( $values[3] ) && in_array( $values[3], $unit_array ) ? $values[3] : '%';
	$fixed           = isset( $values[4] ) && $values[4] ? 1 : 0;

	return array(
		(string)$layout,            // layout            0
		(string)$position,          // position          1
		(int)$height,               // height            2
		(string)$unit,              // unit              3
		(string)$fixed              // fixed             4
	);

}

function tif_sanitize_loop_attr( $values ) {

	// $values = ( null == $values ) ? array() : $values;
	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	$layout_array  = array(
		'none',
		'media_text_1_3',
		'media_text',
		'text',
		'column',
		'inline',
		'cloud',
		'grid_1',
		'grid_2',
		'grid_3',
		'cover',
		'cover_column',
		'cover_media_text',
		'cover_text_media',
		'cover_grid_2',
	);

	$tag_array  = array(
		'h1',
		'h2',
		'h3',
		'h4',
		'h5',
		'h6',
		'div',
		'span',
	);

	$thumb_array  = array(
		'tif_thumb_small',
		'tif_thumb_medium',
		'tif_thumb_large',
		'3.4',
		'2.3',
		'3.5',
		'9.16',
		'1.1',
		'6.5',
		'4.3',
		'3.2',
		'5.3',
		'16.9',
		'2.1',
		'3.1',
		'4.1'
	);

	$layout          = isset( $values[0] ) && in_array( $values[0], $layout_array ) ? $values[0] : 'media_text_1_3';
	$grid            = isset( $values[1] ) ? min(6, max( 0, (int)$values[1] ) ) : 1;
	$thumb           = isset( $values[2] ) && in_array( $values[2], $thumb_array ) ? $values[2] : 'tif_thumb_medium';
	$title           = isset( $values[3] ) ? sanitize_text_field( str_replace('%2C', ',', $values[3] ) ) : null;
	$title_tag       = isset( $values[4] ) && in_array( $values[4], $tag_array ) ? sanitize_text_field( $values[4] ) : 'h2';
	$title_class     = isset( $values[5] ) ? sanitize_text_field( $values[5] ) : null;
	$container_class = isset( $values[6] ) ? sanitize_text_field( $values[6] ) : null;
	$post_class      = isset( $values[7] ) ? sanitize_text_field( $values[7] ) : null;

	return array(
		(string)$layout,            // layout            0
		(int)$grid,                 // grid              1
		(string)$thumb,             // thumb             2
		(string)$title,             // title             3
		(string)$title_tag,         // title_tag         4
		(string)$title_class,       // title class       5
		(string)$container_class,   // container_class   6
		(string)$post_class         // post class        7
	);

}

function tif_sanitize_wrap_attr( $values ) {

	// $values = ( null == $values ) ? array() : $values;
	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	$tag_array  = array(
		'h1',
		'h2',
		'h3',
		'h4',
		'h5',
		'h6',
		'div',
		'p',
		'span',
		'strong',
		'i',
	);

	$content         = isset( $values[0] ) ? sanitize_textarea_field( str_replace('%2C', ',', $values[0] ) ) : null;
	$container_tag   = isset( $values[1] ) && in_array( $values[1], $tag_array ) ? (string)$values[1] : 'div';
	$container_class = isset( $values[2] ) ? (string)$values[2] : null;

	return array(
		$content,                   // content           0
		$container_tag,             // container_tag     1
		$container_class,           // container_class   2
	);

}

function tif_sanitize_scss_variables( $values ) {

	// $values = ( null == $values ) ? array() : $values;
	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	$scss_key  = tif_get_scss_length_array( true );

	$sanitized = array();
	foreach ( $values as $key) {
		if( in_array( $key, $scss_key ) )
			$sanitized[] = $key;
	}

	return ! empty( $sanitized ) ? $sanitized : array();

}

function tif_sanitize_length( $values ) {

	// $values = ( null == $values ) ? array() : $values;
	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	$unit_array  = array(
		'px',
		'pt',
		'em',
		'rem',
		'%',
		'vh',
		'vw',
	);

	$unit   = in_array( end( $values ), $unit_array ) ? array_pop( $values ) : 'px';
	$values = array_map( 'tif_sanitize_float', $values );
	$values = array_merge( $values, array( $unit ) );

	return ! empty( $values ) ? $values : array();

}

function tif_sanitize_sortable( $values ) {

	// $values = ( null == $values ) ? array() : $values;
	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	$value = '';
	$numItems = count($values);
	foreach ( $values as $key => $val ){
		$val = explode( ':', $val );

		$val[0] = sanitize_text_field($val[0]);
		$val[1] = (int)$val[1];

		$value .= implode( ':', $val );

		if ( $key + 1 < $numItems )
			$value .= ',';

	}
	$values = explode( ',', $value );

	return ! empty( $values ) ? $values : array();

}

function tif_sanitize_multiurl( $values ) {

	// $values = ( null == $values ) ? array() : $values;
	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	$value = null;
	$numItems = count($values);
	foreach ( $values as $key => $val ){

		$val = explode( ':', $val );

		$val[0] = sanitize_text_field($val[0]);
		$val[1] = esc_url_raw( urldecode ( $val[1] ) );
		$val[1] = urlencode ( $val[1] );

		$value .= implode( ':', $val );

		if ( $key + 1 < $numItems )
			$value .= ',';

	}
	$values = explode( ',', $value );

	return ! empty( $values ) ? $values : array();

}

function tif_sanitize_array( $values ) {

	// $values = ( null == $values ) ? array() : $values;
	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	array_walk_recursive( $values, function (&$value) {

		$value = filter_var(trim( $value), FILTER_SANITIZE_STRING);

	} );

	return $values;

}

/**
 * Text field sanitization callback.
 *
 * No linebreack allowed
 *
 * @link https://www.php.net/manual/fr/function.htmlentities.php
 * @link https://developer.wordpress.org/reference/functions/sanitize_text_field/
 *
 */
function tif_sanitize_string( $text, $output = false ) {

	// $text = wp_filter_nohtml_kses( $text );

	// if ( isset( $text) && ! $output ) :
	// if ( ! $output ) :
	// 	$text = stripslashes( $text );
	// 	$text = htmlentities( $text, ENT_QUOTES);
	// endif;

	$text = sanitize_text_field( $text );

	return ( $text );

}

/**
 * Textarea field sanitization callback.
 *
 * @link https://www.php.net/manual/fr/function.htmlentities.php
 * @link https://developer.wordpress.org/reference/functions/sanitize_textarea_field/
 *
 */
function tif_sanitize_textarea( $text ) {

	$text = sanitize_textarea_field( $text );

	return ( $text );

}

/**
 * Textarea field sanitization callback.
 *
 * @link https://www.php.net/manual/fr/function.htmlentities.php
 * @link https://developer.wordpress.org/reference/functions/sanitize_textarea_field/
 *
 */
function tif_sanitize_nohtml( $text, $output = false ) {

	$text = wp_filter_nohtml_kses( $text );

	// if ( isset( $text) && ! $output ) :
	if ( ! $output ) :
		$text = html_entity_decode( $text );
		$text = htmlentities( $text, ENT_QUOTES);
	endif;

	$text = sanitize_textarea_field( $text );

	return ( $text );

}

function tif_sanitize_html( $html ) {

	$array = array(
		'a' => array(
			'href' => array(),
			'title' => array(),
			'alt' => array()
		),
		'p' => array(),
		'br' => array(),
		'em' => array(),
		'span' => array(),
		'strong' => array(),
	);

	return wp_kses ( $html, $array );

}

/**
 * CSS sanitization callback.
 *
 * NOTE: wp_strip_all_tags() can be passed directly as `$wp_customize->add_setting()`
 * 'sanitize_callback'. It is wrapped in a callback here merely for example purposes.
 *
 * @link wp_strip_all_tags() https://developer.wordpress.org/reference/functions/wp_strip_all_tags/
 *
 * @param string $css CSS to sanitize.
 * @return string Sanitized CSS.
 */
function tif_sanitize_css( $css ) {

	$css = str_replace( '_', '-', $css );

	return wp_strip_all_tags( $css );

}

function tif_sanitize_float( $input, $setting = '' ) {

	$input = str_replace( ',', '.', $input);
	$float = filter_var( $input, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );

	$default = null != $setting ? $setting->default : '' ;
	// return ( (float)$float ? $float : $default );
	return floatval($float);

}

/**
 * Number int sanitization callback.
 *
 * @param int				  $number  Number to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return int Sanitized number; otherwise, the setting default.
 */
function tif_sanitize_int( $number, $setting = '' ) {
	// Ensure input is an absolute integer.
	$number = (int)$number;

	// Get the input attributes associated with the setting.
	if ( null != $setting )
		$atts = $setting->manager->get_control( $setting->id )->input_attrs;

	// Get minimum number in the range.
	$min = ( isset( $atts['min'] ) ? $atts['min'] : $number );

	// Get maximum number in the range.
	$max = ( isset( $atts['max'] ) ? $atts['max'] : $number );

	// Get step.
	$step = ( isset( $atts['step'] ) ? $atts['step'] : 1 );

	// If the number is within the valid range, return it; otherwise, return the default
	$default = null != $setting ? $setting->default : '' ;
	return ( $min <= $number && $number <= $max && is_int( (int)$number / (int)$step ) ? $number : $default );

}

/**
 * Number absint sanitization callback.
 *
 * @link absint() https://developer.wordpress.org/reference/functions/absint/
 *
 * @param int				  $number  Number to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return int Sanitized number; otherwise, the setting default.
 */
function tif_sanitize_absint( $number, $setting = '' ) {
	// Ensure input is an absolute integer.
	$number = absint( $number );

	// Get the input attributes associated with the setting.
	if ( null != $setting )
		$atts = $setting->manager->get_control( $setting->id )->input_attrs;

	// Get minimum number in the range.
	$min = ( isset( $atts['min'] ) ? $atts['min'] : $number );

	// Get maximum number in the range.
	$max = ( isset( $atts['max'] ) ? $atts['max'] : $number );

	// Get step.
	$step = ( isset( $atts['step'] ) ? $atts['step'] : 1 );

	// If the number is within the valid range, return it; otherwise, return the default
	$default = null != $setting ? $setting->default : '' ;
	return ( $min <= $number && $number <= $max && is_int( (int)$number / (int)$step ) ? $number : $default );

}

/**
 * Number Range sanitization callback.
 *
 * @link absint() https://developer.wordpress.org/reference/functions/absint/
 *
 * @param int				  $number  Number to check within the numeric range defined by the setting.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return int|string The number, if it is zero or greater and falls within the defined range; otherwise,
 *					the setting default.
 */
function tif_sanitize_number_range( $number, $setting = '' ) {

	// Ensure input is an absolute integer.
	$number = absint( $number );

	// Get the input attributes associated with the setting.
	if ( null != $setting )
		$atts = $setting->manager->get_control( $setting->id )->input_attrs;

	// Get minimum number in the range.
	$min = ( isset( $atts['min'] ) ? $atts['min'] : $number );

	// Get maximum number in the range.
	$max = ( isset( $atts['max'] ) ? $atts['max'] : $number );

	// Get step.
	$step = ( isset( $atts['step'] ) ? $atts['step'] : 1 );

	// If the number is within the valid range, return it; otherwise, return the default
	$default = null != $setting ? $setting->default : '' ;
	return ( $min <= $number && $number <= $max && is_int( $number / $step ) ? $number : $default );

}

/**
 * Drop-down Pages sanitization callback.
 *
 * Sanitization callback for 'dropdown-pages' type controls. This callback sanitizes `$page_id`
 * as an absolute integer, and then validates that $input is the ID of a published page.
 *
 * @link absint() https://developer.wordpress.org/reference/functions/absint/
 * @link get_post_status() https://developer.wordpress.org/reference/functions/get_post_status/
 *
 * @param int				  $page	Page ID.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return int|string Page ID if the page is published; otherwise, the setting default.
 */
function tif_sanitize_dropdown_pages( $page_id, $setting = '' ) {
	// Ensure $input is an absolute integer.
	$page_id = absint( $page_id );

	// If $page_id is an ID of a published page, return it; otherwise, return the default.
	$default = null != $setting ? $setting->default : '' ;
	return ( 'publish' == get_post_status( $page_id ) ? $page_id : $default );
}

function tif_sanitize_dropdown_category( $cat_id ) {

	$cat_id = ! is_array( $cat_id ) ? explode( ',', $cat_id ) : $cat_id;

	array_map( 'intval', $cat_id );

	return $cat_id;

}

// if ( ! function_exists( 'tif_sanitize_dropdown_category' ) ) {
//
// 	function tif_sanitize_dropdown_category( $cat_id ) {
// 		// Ensure $input is an absolute integer.
// 		$cat_id = absint( $cat_id );
//
// 		// If $page_id is an ID of a published page, return it; otherwise, return the default.
// 		return ( get_term_by( 'id', $cat_id, 'category') ? $cat_id : get_option( 'default_category' ) );
// 	}
//
// }
/**
 * Email sanitization callback.
 *
 * Sanitization callback for 'email' type text controls. This callback sanitizes `$email`
 * as a valid email address.
 *
 * @link sanitize_email() https://developer.wordpress.org/reference/functions/sanitize_key/
 * @link sanitize_email() https://developer.wordpress.org/reference/functions/sanitize_email/
 *
 * @param string			   $email   Email address to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string The sanitized email if not null; otherwise, the setting default.
 */
function tif_sanitize_email( $email, $setting = '' ) {
	// Strips out all characters that are not allowable in an email address.
	$email = sanitize_email( $email );

	// If $email is a valid email, return it; otherwise, return the default.
	$default = null != $setting ? $setting->default : '' ;
	return ( ! is_null( $email ) ? $email : $default );
}

/**
 * HEX Color sanitization callback.
 *
 * @link sanitize_hex_color() https://developer.wordpress.org/reference/functions/sanitize_hex_color/
 * @link sanitize_hex_color_no_hash() https://developer.wordpress.org/reference/functions/sanitize_hex_color_no_hash/
 *
 * @param string			   $hex_color HEX color to sanitize.
 * @param WP_Customize_Setting $setting   Setting instance.
 * @return string The sanitized hex color if not null; otherwise, the setting default.
 */
function tif_sanitize_hexcolor( $hex_color, $setting = '' ) {
	// Sanitize $input as a hex value without the hash prefix.
	$hex_color = sanitize_hex_color( $hex_color );

	// If $input is a valid hex value, return it; otherwise, return the default.
	$default = null != $setting ? $setting->default : '' ;
	return ( ! is_null( $hex_color ) ? $hex_color : $default );
}

/**
 * [tif_sanitize_keycolor description]
 * @param  [type] $key_color               [description]
 * @param  string $setting                 [description]
 * @return [type]            [description]
 * @TODO
 */
function tif_sanitize_keycolor( $key_color, $setting = '' ) {

	$key_array  = array(
		'light',		// Theme palette
		'light_accent',
		'primary',
		'dark_accent',
		'dark',
		'default',		// Semantic colors
		'info',
		'success',
		'warning',
		'danger',
		'white',		// Generic colors
		'black',
		'transparent',
		'none'
	);


	if ( strpos( $key_color, '#' ) !== false )
		$key_color = tif_sanitize_hexcolor( $key_color );	// hex color
	else
		$key_color = in_array( $key_color, $key_array ) ? $key_color : null;		// key color

	// If $input is a valid key or hex value, return it; otherwise, return the default.
	$default = null != $setting ? $setting->default : '' ;
	return ( ! is_null( $key_color ) ? $key_color : $default );
}


/**
 * [tif_sanitize_array_keycolor description]
 * @param  [type] $values                [description]
 * @param  string $setting               [description]
 * @return [type]          [description]
 * @TODO
 */
function tif_sanitize_array_keycolor( $values, $setting = '' ) {

	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	$brightness_array  = array(
		'lightest',
		'lighter',
		'light',
		'normal',
		'dark',
		'darker',
		'darkest'
	);

	$tmp = array();

	$tmp[0] = tif_sanitize_keycolor( $values[0] );														// color
	$tmp[1] = isset( $values[1] ) && in_array( $values[1], $brightness_array ) ? $values[1] : 'normal';	// brightness
	$tmp[2] = isset( $values[2] ) ? max( -1, min( 1, tif_sanitize_float( $values[2] ) ) ) : '1';		// opacity

	return (array)$tmp;
}

/**
 * [tif_sanitize_array_boxshadow description]
 * @param  [type] $values               [description]
 * @return [type]         [description]
 * @TODO
 */
function tif_sanitize_array_boxshadow( $values ) {

	// $values = ( null == $values ) ? array() : $values;
	$values = ! is_array( $values ) ? explode( ',', $values ) : $values;

	$position_x = isset( $values[0] ) ? tif_sanitize_float( $values[0] ) : false;
	$position_y = isset( $values[1] ) ? tif_sanitize_float( $values[1] ) : false;
	$blur       = isset( $values[2] ) ? tif_sanitize_float( $values[2] ) : false;
	$spread     = isset( $values[3] ) ? tif_sanitize_float( $values[3] ) : false;
	$key_color  = isset( $values[4] ) ? tif_sanitize_keycolor( $values[4] ) : 'black' ;
	$opacity    = isset( $values[5] ) ? tif_sanitize_float( $values[5] ) : false;
	$inset      = isset( $values[6] ) && $values[6] == 'inset' ? 'inset' : null;

	return array(
		(float)$position_x,
		(float)$position_y,
		(float)$blur,
		(float)$spread,
		(string)$key_color,
		(float)$opacity,
		(string)$inset
	);

}

/**
 * Image sanitization callback.
 *
 * @link wp_check_filetype() https://developer.wordpress.org/reference/functions/wp_check_filetype/
 *
 * @param string			   $image   Image filename.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string The image filename if the extension is allowed; otherwise, the setting default.
 */
function tif_sanitize_image( $image, $setting = '' ) {
	/*
	 * Array of valid image file types.
	 *
	 * The array includes image mime types that are included in wp_get_mime_types()
	 */
	$mimes = array(
		'jpg|jpeg|jpe' => 'image/jpeg',
		'gif'          => 'image/gif',
		'png'          => 'image/png',
		'bmp'          => 'image/bmp',
		'tif|tiff'     => 'image/tiff',
		'ico'          => 'image/x-icon'
	);
	// Return an array with file extension and mime_type.
	$file = wp_check_filetype( $image, $mimes );
	// If $image has a valid mime_type, return it; otherwise, return the default.
	$default = null != $setting ? $setting->default : '' ;
	return ( $file['ext'] ? $image : $default );
}

/**
 * Select sanitization callback.
 *
 * @link sanitize_key()			   https://developer.wordpress.org/reference/functions/sanitize_key/
 * @link $wp_customize->get_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/get_control/
 *
 * @param string			   $input   Slug to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
 */
function tif_sanitize_select( $input, $setting = '' ) {

	// Ensure input is a slug.
	$input = sanitize_key( $input );

	// Get list of choices from the control associated with the setting.
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// If the input is a valid key, return it; otherwise, return the default.
	$default = null != $setting ? $setting->default : '' ;
	return ( array_key_exists( $input, $choices ) ? $input : $default );

}

/**
 * URL sanitization callback.
 *
 * @link esc_url_raw() https://developer.wordpress.org/reference/functions/esc_url_raw/
 *
 * @param string $url URL to sanitize.
 * @return string Sanitized URL.
 */
function tif_sanitize_url( $url ) {

	return esc_url_raw( $url );

}
